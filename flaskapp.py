from flask import Flask
app = Flask(__name__)


@app.route("/")
def index():
    return ('<!DOCTYPE html>\n'
            '<meta charset="utf-8">\n<title>Flask demo</title>\n'
            '<h1>This is a minimal Flask demo application!</h1>\n<img src="/static/hello.png">\n')
